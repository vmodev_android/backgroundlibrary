package com.vmodev.modules.backgroundservice;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import android.content.Context;
import com.vmodev.modules.backgroundservice.actions.TimerAction;

public class TimerActionManager implements ITimerActionManager {
	private static TimerActionManager instant;
	private Map<String, Timer> timersMap;
	private Context context;

	private TimerActionManager() {
		timersMap = new HashMap<String, Timer>();
	}

	public static ITimerActionManager getManager() {
		if (instant == null) {
			instant = new TimerActionManager();
		}
		return instant;
	}

	public void create(Context context) {
		this.context = context;
		VLog.v("Created Timer Action Manager");
	}

	public void destroy() {
		this.context = null;
		Collection<Timer> values = timersMap.values();
		for (Timer timer : values) {
			timer.cancel();
		}
		timersMap.clear();
		VLog.v("Destroyed Timer Action Manager");
	}

	public void cancelAction(String keyTimer) {
		Timer timer = timersMap.get(keyTimer);
		if (timer != null) {
			timer.cancel();
			timersMap.remove(keyTimer);
		}
	}

	@Override
	public void scheduleAction(String keyTimer, final TimerAction<?> action,
			long delay) {
		if (preExecute(action)) {
			Timer timer = new Timer(keyTimer);
			timer.schedule(new TimerTask() {
				@Override
				public void run() {
					try {
						action.execute();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}, delay);
			putTimerTaskMap(keyTimer, timer);
		}
	}

	@Override
	public void scheduleAction(String keyTimer, final TimerAction<?> action,
			Date when) {
		if (preExecute(action)) {
			Timer timer = new Timer(keyTimer);
			timer.schedule(new TimerTask() {
				@Override
				public void run() {
					try {
						action.execute();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}, when);
			putTimerTaskMap(keyTimer, timer);
		}
	}

	@Override
	public void scheduleAction(String keyTimer, final TimerAction<?> action,
			long delay, long duration) {
		if (preExecute(action)) {
			Timer timer = new Timer(keyTimer);
			timer.schedule(new TimerTask() {
				@Override
				public void run() {
					try {
						action.execute();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}, delay, duration);
			putTimerTaskMap(keyTimer, timer);
		}
	}

	@Override
	public void scheduleAction(String keyTimer, final TimerAction<?> action,
			Date when, long delay) {
		if (preExecute(action)) {
			Timer timer = new Timer(keyTimer);
			timer.schedule(new TimerTask() {
				@Override
				public void run() {
					try {
						action.execute();
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}, when, delay);
			putTimerTaskMap(keyTimer, timer);
		}
	}

	private boolean preExecute(TimerAction<?> action) {
		if (context == null) {
			VLog.e("Don't allow execute action without service");
			return false;
		} else {
			action.setContext(context);
			return true;
		}
	}

	private void putTimerTaskMap(String keyTimer, Timer timer) {
		Timer oldTimer = timersMap.get(keyTimer);
		if (oldTimer != null) {
			oldTimer.cancel();
			timersMap.remove(keyTimer);
		}
		timersMap.put(keyTimer, timer);
	}
}
