package com.vmodev.modules.backgroundservice.actions;

public abstract class Action<Result> {
	private Result result;

	public Action() {
	}

	protected abstract Result doInBackground() throws Exception;

	public final void execute() throws Exception {
		result = doInBackground();
	}

	public final Result getResult() {
		return result;
	}

}
