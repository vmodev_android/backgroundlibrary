package com.vmodev.backgroundservicesample;

import android.util.Log;

class VLog {
	private static final String TAG = "Background Module";
	protected static boolean debug = true;

	public static void v(String message) {
		if (debug) {
			Log.v(TAG, message);
		}
	}

	public static void d(String message) {
		if (debug) {
			Log.d(TAG, message);
		}
	}

	public static void i(String message) {
		if (debug) {
			Log.i(TAG, message);
		}
	}

	public static void e(String message) {
		if (debug) {
			Log.e(TAG, message);
		}
	}
}
