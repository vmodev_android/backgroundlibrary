package com.vmodev.backgroundservicesample;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.vmodev.backgroundservicelibrary.R;
import com.vmodev.modules.backgroundservice.Callback;
import com.vmodev.modules.backgroundservice.TimerActionManager;
import com.vmodev.modules.backgroundservice.actions.Action;

public class MainActivity extends BaseActivity implements OnClickListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		findViewById(R.id.button1).setOnClickListener(this);
		findViewById(R.id.button2).setOnClickListener(this);
		findViewById(R.id.button3).setOnClickListener(this);
		findViewById(R.id.button4).setOnClickListener(this);
		findViewById(R.id.button5).setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.button1:
			getManager().execute(new DemoAction(),
					new DemoGetResponseListener());
			break;
		case R.id.button2:
			Intent intent = new Intent(getApplicationContext(),
					AgentTimerService.class);
			startService(intent);
			break;
		case R.id.button3:
			Intent name = new Intent(getApplicationContext(),
					AgentTimerService.class);
			stopService(name);
			break;
		case R.id.button4:
			TimerActionManager.getManager().scheduleAction("timer",
					new SampleTimerAction(), 1000, 5000);
			break;
		case R.id.button5:
			TimerActionManager.getManager().cancelAction("timer");
			break;
		}
	}

	class DemoGetResponseListener implements Callback<String, DemoAction> {

		@Override
		public void onFailureRequest(DemoAction action,
				final Exception exception) {
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					Toast.makeText(getApplication(), exception.getMessage(),
							Toast.LENGTH_LONG).show();
				}
			});
		}

		@Override
		public void onSuccessRequest(DemoAction action, String result) {
			final String res = (String) action.getResult();
			runOnUiThread(new Runnable() {

				@Override
				public void run() {
					Toast.makeText(getApplication(), res, Toast.LENGTH_LONG)
							.show();
				}
			});
		}

	}

	private class DemoAction extends Action<String> {
		@Override
		protected String doInBackground() {
			try {
				Thread.sleep(5000);
				throw new RuntimeException("Run time exception");
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			return "ok";
		}
	}

}
