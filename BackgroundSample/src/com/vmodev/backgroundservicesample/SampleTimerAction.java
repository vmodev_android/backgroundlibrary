package com.vmodev.backgroundservicesample;

import com.vmodev.modules.backgroundservice.actions.TimerAction;

public class SampleTimerAction extends TimerAction<Void> {

	@Override
	protected Void doInBackground() {
		VLog.i("Sync data");
		return null;
	}

}
