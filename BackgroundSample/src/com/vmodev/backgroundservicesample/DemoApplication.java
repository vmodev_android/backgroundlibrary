package com.vmodev.backgroundservicesample;

import android.app.Application;

import com.vmodev.modules.backgroundservice.Config;

public class DemoApplication extends Application {

	@Override
	public void onCreate() {
		super.onCreate();
		Config.setup(getApplicationContext());
	}
}
