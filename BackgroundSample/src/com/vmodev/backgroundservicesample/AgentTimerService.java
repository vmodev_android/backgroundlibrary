package com.vmodev.backgroundservicesample;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import com.vmodev.modules.backgroundservice.TimerActionManager;

public class AgentTimerService extends Service {

	@Override
	public void onCreate() {
		super.onCreate();
		TimerActionManager.getManager().create(getApplicationContext());
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		TimerActionManager.getManager().destroy();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		return START_STICKY;
	}

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

}
