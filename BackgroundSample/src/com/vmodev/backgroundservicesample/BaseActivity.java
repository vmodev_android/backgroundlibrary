package com.vmodev.backgroundservicesample;

import com.vmodev.modules.backgroundservice.ClientManager;

import android.app.Activity;
import android.os.Bundle;

public class BaseActivity extends Activity {
	private ClientManager clientManager = new ClientManager();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		clientManager.start(this);
	}

	protected ClientManager getManager() {
		return clientManager;
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		clientManager.release();
	}
}
